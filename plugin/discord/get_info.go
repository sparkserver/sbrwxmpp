// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package discord

import (
	"encoding/json"
	"fmt"
	"strings"
)

type linkedPersonaInfo struct {
	PersonaID uint64 `json:"personaId"`
	UserID    uint64 `json:"userId"`
	Name      string `json:"name"`
}

func (p *Plugin) getInfoByDiscordID(discordID string) (*linkedPersonaInfo, error) {
	resp, err := p.client.Get(strings.ReplaceAll(p.cfg.InfoURL, "%", discordID))
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("InfoURL returned http status %v", resp.StatusCode)
	}

	info := new(linkedPersonaInfo)
	if err := json.NewDecoder(resp.Body).Decode(info); err != nil {
		return nil, err
	}
	return info, nil
}
