// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package discord

type Config struct {
	Enabled          bool
	Token            string
	AvatarURL        string `toml:"avatar_url"`
	InfoURL          string `toml:"info_url"`
	NotLinkedMessage string `toml:"not_linked_message"`
	QueueSize        int    `toml:"queue_size"`
	Channels         map[string]string
}
