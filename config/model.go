// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package config

import (
	"github.com/BurntSushi/toml"
	"github.com/rs/zerolog"
)

type Config struct {
	Metadata toml.MetaData `toml:"-"`

	Addr           string
	Cert           string
	CertKey        string
	WriteQueueSize int `toml:"write_queue_size"`
	API            APIConfig
	Webhook        WebhookConfig
	Logging        LoggingConfig
	Plugins        map[string]toml.Primitive
}

func (c Config) DecodePluginConfig(name string, i interface{}) error {
	return c.Metadata.PrimitiveDecode(c.Plugins[name], i)
}

type LoggingLevel struct {
	zerolog.Level
}

func (l *LoggingLevel) UnmarshalText(text []byte) error {
	var err error
	l.Level, err = zerolog.ParseLevel(string(text))
	return err
}

type LoggingConfig struct {
	Pretty bool
	API    LoggingLevel
	XMPP   LoggingLevel
}

type APIConfig struct {
	Addr string
	Key  string
}

type WebhookConfig struct {
	Target string
	Secret string
}
