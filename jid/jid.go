// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package jid

import "strings"

func parseFirstPart(part string) (string, string) {
	idx := strings.IndexByte(part, '@')
	if idx == -1 {
		return "", part
	}
	return part[:idx], part[idx+1:]
}

func Parse(jid string) JID {
	resIndex := strings.IndexByte(jid, '/')
	var local, domain, resource string
	if resIndex == -1 {
		local, domain = parseFirstPart(jid)
	} else {
		local, domain = parseFirstPart(jid[:resIndex])
		resource = jid[resIndex+1:]
	}
	return JID{
		Local:    local,
		Domain:   domain,
		Resource: resource,
	}
}

type JID struct {
	Local    string
	Domain   string
	Resource string
}

func (jid JID) String() string {
	var b strings.Builder
	b.Grow(len(jid.Local) + len(jid.Domain) + len(jid.Resource) + 2)
	if jid.Local != "" {
		b.WriteString(jid.Local)
		b.WriteByte('@')
	}
	b.WriteString(jid.Domain)
	if jid.Resource != "" {
		b.WriteByte('/')
		b.WriteString(jid.Resource)
	}
	return b.String()
}

func (jid JID) Bare() JID {
	jid.Resource = ""
	return jid
}

func (jid JID) WithResource(resource string) JID {
	jid.Resource = resource
	return jid
}

func (jid JID) WithDomain(domain string) JID {
	jid.Domain = domain
	return jid
}

// BareEqual returns true if the supplied JIDs are equal, ignoring resourcepart
func BareEqual(a, b JID) bool {
	return (a.Local == b.Local) && (a.Domain == b.Domain)
}
